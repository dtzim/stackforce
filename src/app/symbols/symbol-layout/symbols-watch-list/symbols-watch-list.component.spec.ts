import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SymbolsWatchListComponent } from './symbols-watch-list.component';

describe('SymbolsWatchListComponent', () => {
  let component: SymbolsWatchListComponent;
  let fixture: ComponentFixture<SymbolsWatchListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SymbolsWatchListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SymbolsWatchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
