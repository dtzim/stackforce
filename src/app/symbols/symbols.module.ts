import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SymbolLayoutComponent } from './symbol-layout/symbol-layout.component';
import { SymbolsRoutingModule } from './symbols-routing.module';
import { FilterComponent } from './symbol-layout/filter/filter.component';
import { AllSymbolsComponent } from './symbol-layout/all-symbols/all-symbols.component';
import { SymbolsWatchListComponent } from './symbol-layout/symbols-watch-list/symbols-watch-list.component';



@NgModule({
  declarations: [SymbolLayoutComponent, FilterComponent, AllSymbolsComponent, SymbolsWatchListComponent],
  imports: [
	CommonModule,
	FormsModule,
	SymbolsRoutingModule
  ]
})
export class SymbolsModule { }
