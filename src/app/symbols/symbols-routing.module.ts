import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SymbolLayoutComponent } from './symbol-layout/symbol-layout.component';

const routes: Routes = [
	{ path: '', component: SymbolLayoutComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
  })

export class SymbolsRoutingModule {

}
