import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { WatchListComponent } from './dashboard-layout/watch-list/watch-list.component';
import { DashboardSymbolsComponent } from './dashboard-layout/dashboard-symbols/dashboard-symbols.component';

@NgModule({
  declarations: [DashboardLayoutComponent, WatchListComponent, DashboardSymbolsComponent],
  imports: [
	CommonModule,
	FormsModule,
	DashboardRoutingModule
  ]
})
export class DashboardModule { }
